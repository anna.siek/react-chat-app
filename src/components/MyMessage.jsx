import MessageForm from "./MessageForm";

const MyMessage = ({message}) => {
    if (message?.attachments?.length > 0) {
        return (
            <img
                src={MessageForm.attachments[0].file}
                alt="message-attachment"
                className="message-image"
                style={{ float: 'right' }}
            />
        )
    }
    return (
        <div className="message message--my" style={{ float: 'right', marginRight: '20px',backgroundColor:'green', color: '#fff' }}>
            {message.text}
        </div>
    );
}

export default MyMessage;